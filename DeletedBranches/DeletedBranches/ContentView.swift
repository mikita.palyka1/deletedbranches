//
//  ContentView.swift
//  DeletedBranches
//
//  Created by Mikita Palyka on 6.02.24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("Hello, world!")
            Text("New features text")
            Text("some changes")
            
            
            Text("One more restore")
            Text("4-th rebase commit")
            Text("5-th rebase commit")
            
            Text("xyu")
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
