//
//  DeletedBranchesApp.swift
//  DeletedBranches
//
//  Created by Mikita Palyka on 6.02.24.
//

import SwiftUI

@main
struct DeletedBranchesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
